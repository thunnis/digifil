import xlrd,csv,codecs

book = xlrd.open_workbook('to_correct.xls')

for sheet in book.sheets():
    fn = "%s.csv" % sheet.name
    data=[]
    for row_idx in range(1, sheet.nrows):
        attrib = str(sheet.cell(row_idx, 0).value)
        attrib2 = str(sheet.cell(row_idx, 1).value)
        data.append([attrib, attrib2])
    with codecs.open('pos_training_files/%s' % fn, 'w', encoding='utf-8') as f:
        w=csv.writer(f, delimiter=';')
        w.writerows(data)
    print("Wrote", fn)
